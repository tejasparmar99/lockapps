package com.example.tejas.lock;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DELL on 18-10-2017.
 */

public class AppsListAdapter extends RecyclerView.Adapter<AppsListAdapter.ViewHolder>  {

    private ArrayList<AppsList> list;
    private Context context;

    public AppsListAdapter(ArrayList<AppsList> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.apps_adapter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AppsList appsList = list.get(position);

        holder.appName.setText(appsList.getAppName());
        holder.appIcon.setImageDrawable(appsList.getAppIcon());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView appName;
        public ImageView appIcon;
        public ViewHolder(View itemView) {
            super(itemView);

            appName = (TextView)itemView.findViewById(R.id.app_name);
            appIcon = (ImageView)itemView.findViewById(R.id.app_icon);
        }
    }
}
