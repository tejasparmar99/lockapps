package com.example.tejas.lock;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private static final String USERNAME = "Tejas";
    private static final String PASSWORD = "tejas123";

    // UI references.
    private EditText userName;
    private EditText password;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

        userName = (EditText)findViewById(R.id.user_name);
        password = (EditText)findViewById(R.id.password);

        login = (Button)findViewById(R.id.login_button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginButtonClicked(view);
            }
        });

        Intent i = new Intent(this, BackGroundService.class);
        startService(i);
    }

    public void LoginButtonClicked(View view){
        if(userName.getText().toString().equals(USERNAME) && password.getText().toString().equals(PASSWORD)){
            Intent i = new Intent(LoginActivity.this, AppsListActivity.class);
            startActivity(i);
        }
        else{
            Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
            userName.setText("");
            password.setText("");
            userName.requestFocus();
        }
    }
}

