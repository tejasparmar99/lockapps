package com.example.tejas.lock;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by DELL on 19-10-2017.
 */

public class AppDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "appStatus.db";
    private static final String TABLE_NAME = "applicationStatus";
    private static final String COLUMN_ID = "appId";
    private static final String COLUMN_NAME = "appName";
    private static final String COLUMN_STATUS = "appStatus";

    public AppDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT "
                + COLUMN_NAME + " TEXT "
                + COLUMN_STATUS + " TEXT "
                + ");";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME );
        onCreate(sqLiteDatabase);
    }

    public void addApp(App app){
        ContentValues contentValues  = new ContentValues();
        contentValues.put(COLUMN_NAME, app.get_appName());
        contentValues.put(COLUMN_STATUS, app.get_status());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_NAME, null, contentValues);
        db.close();
    }

    public void removeApp(String appName){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + "=\"" + appName + "\"");
    }
}
