package com.example.tejas.lock;

import android.graphics.drawable.Drawable;

/**
 * Created by DELL on 18-10-2017.
 */

public class AppsList {

    private String appName;
    private Drawable appIcon;

    public AppsList(String appName, Drawable appIcon) {
        this.appName = appName;
        this.appIcon = appIcon;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public String getAppName() {
        return appName;
    }

}
