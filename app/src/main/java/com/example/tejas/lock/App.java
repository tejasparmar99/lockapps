package com.example.tejas.lock;

/**
 * Created by DELL on 19-10-2017.
 */

public class App {
    private int _id;
    private String _appName;
    private String _status;

    public App(String _appName, String _status) {
        this._appName = _appName;
        this._status = _status;
    }

    public String get_appName() {
        return _appName;
    }

    public String get_status() {
        return _status;
    }
}
